FROM debian:stable
RUN apt-get update
RUN apt-get install -y --no-install-recommends cmake arduino-mk wget ca-certificates xz-utils
RUN cd /root && wget -nv https://downloads.arduino.cc/arduino-1.6.13-linux64.tar.xz
RUN cd /root && tar xJf arduino-1.6.13-linux64.tar.xz
